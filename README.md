# AirPlay Buddy

<p align="center">
  <img width="400" height="261" src="Assets/logo.png?raw=true">
</p>

AirPlay Buddy is a managed "screensaver" App for tvOS and an alternative to Apple's Conference Room Display only mode. 

With AirPlay Buddy you can:
* Run the App in single App mode and automate your processes for a zero-touch deployment.
* Give content curators access to managing the background image playlist.
* Allow your Apple TV to double as digital signage and an AirPlay device.

It is currently tested and working with Jamf Pro but should also work with other MDM providers.

##### Sample Screenshot:
<img src="Assets/screenshot_example.png?raw=true" width="650">


### Installation

A build of AirPlay Buddy is available for free in the App Store. You can fork the project and distribute it as Custom App as well or as an enterprise App.



### Managed App Configuration Preferences <br />
App configuration Preferences are currently required to load the CSV file location through the "de.fau.rrze.AirPlayBuddy.dataURL" preferences key. All available keys are shown here: <br />

**de.fau.rrze.AirPlayBuddy.dataURL** (required) <br />
URL of the CSV file containing image information. There is no default option for this key and the App will not function as built without this value loaded. <br />
**de.fau.rrze.AirPlayBuddy.imageTimer** (optional) <br />
Time (in seconds) of the image timer default. Image display time length is specified in the CSV file but will default to this option if nothing is entered in the file. <br />
**de.fau.rrze.AirPlayBuddy.airplayViewHide** (optional) <br />
Boolean value set to false by default, override with a "true" value to hide the floating AirPlay box. <br />
**de.fau.rrze.AirPlayBuddy.disableAirplayBoxMovement** (optional) <br />
Boolean value set to false by default, override with a "true" value to hide disable the movement of the Airplay box. <br />
**de.fau.rrze.AirPlayBuddy.shuffleImages** (optional) <br />
Boolean value to define if the list of images will be shuffled on load. Defaults to true.<br />
**de.fau.rrze.AirPlayBuddy.dataCheckTimer** (optional) <br />
Time (in seconds) between checks for updates in the CSV file. The default time is set to 180 seconds. <br />
**de.fau.rrze.AirPlayBuddy.imageTimer** (optional) <br />
Time (in seconds) of the image timer default. Image display time length is specified in the CSV file but will default to this option if nothing is entered in the file. <br />
**de.fau.rrze.AirPlayBuddy.deviceName** (optional) <br />
With tvOS 16 Apple introduced new privacy seting. One of them forbids developers to get the name of the device. To connect the Apple TV via AirPlay the name of the device is needed. With this parameter the deviceName can be set via MDM profile.<br />
**de.fau.rrze.AirPlayBuddy.defaultBackground** (optional) <br />
This value can be set to "DefaultBackgroundNoLogo" in order to remove the MDM Image Viewer logo from the default background image. <br />
**de.fau.rrze.AirPlayBuddy.defaultDescription** (optional) <br />Replace the AirPlay description with a custom one. <br />
**de.fau.rrze.AirPlayBuddy.defaultSubtitle** (optional) <br />Replace the AirPlay subtitle "CHOOSE THIS APPLE TV" with a custom one. <br />
**de.fau.rrze.AirPlayBuddy.showClock** (optional) <br />
Boolean value set to true by default to define if the current time is shown up in the Air Play floating window.<br />
**de.fau.rrze.AirPlayBuddy.avatarShowPresence** (optional) <br/>
Boolean value set to false by default to define if a json file with "presence" information for users should be loaded and shown<br/>
**de.fau.rrze.AirPlayBuddy.avatarSizeX** (optional), **de.fau.rrze.AirPlayBuddy.avatarSizeY** (optional) <br/>
double value to define x and y size of user's avatar if presence is displayed (defaults to 90.0 x 90.0)a<br/>
**de.fau.rrze.AirPlayBuddy.avatarFrameSizeX** (optional), **de.fau.rrze.AirPlayBuddy.avatarFrameSizeY** (optoional) <br>/
double value to define x and y size of the frame which contains user's avatar (defaults to 110.0 x 100.0) <br/>
**de.fau.rrze.AirPlayBuddy.avatarUserStatusURL** (optional) <br/>
Download URL for the json file with user's presence status <br/>
**de.fau.rrze.AirPlayBuddy.avatarPresenceFetchTimer** (optional) <br/>
Time in seconds until new presence data is loaded <br/>


*Sample configuration preferences:*
```xml
<dict>
    <key>de.fau.rrze.AirPlayBuddy.dataURL</key>
    <string>https://link.to.preferences.csv</string>
    <key>de.fau.rrze.AirPlayBuddy.imageTimer</key>
    <integer>10</integer>
    <key>de.fau.rrze.AirPlayBuddy.disableAirplayBoxMovement</key>
    <true/>
    <key>de.fau.rrze.AirPlayBuddy.shuffleImages</key>
    <true/>
    <key>de.fau.rrze.AirPlayBuddy.airplayViewTimer</key>
    <integer>25</integer>
    <key>de.fau.rrze.AirPlayBuddy.dataCheckTimer</key>
    <integer>600</integer>
    <key>de.fau.rrze.AirPlayBuddy.defaultSubtitle</key>
    <string>SELECT THIS APPLE TV</string>
    <key>de.fau.rrze.AirPlayBuddy.defaultDescription</key>
    <string>New AirPlay description</string>
</dict>
```

*Samble JSON data for user's presence
```json
[
    {
        "id": "a49c4082-5cd5-4178-a305-dce06dd702c0",
        "name": "Fritz",
        "image": "https://URL-TO-AVATAR.COM/memoji3.png",
        "status": 1,
        "presence": 1,
    },
    {
        "id": "44847a41-c0f8-2dda-51db-a423w80cb01c",
        "name": "Alexander",
        "image": "https://URL-TO-AVATAR.COM/memoji3.png",
        "presence": 2,
        "status": 3
    },
    {
        "id": "44847a41-c0f8-3dda-51db-a423w80cb01c",
        "name": "Alexander",
        "image": "https://URL-TO-AVATAR.COM/memoji3.png",
        "presence": 0,
        "status": 0
    }
]
```
where:
- `id` is a random UUID
- `name` is the name of the user
- `image` URL to the user's avatar image
- `status` integer, defines status of the user: 0 = offline, 1 = free, 2 = busy, 3 = do not disturb
- `presence` integer, defines the presence of the user: 0 = offline, 1 = at the office, 2 = home office

### CSV Image Playlist Settings Example
The CSV (",") file liked in the managed preferences should show values including a header and in the order listed below. The "UpdateInterval" and "Cache" values are not currently used but should still be included within the CSV file.

| Name | URL | Duration | Image Description | StartOn | EndBy | Cache |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| Test Image | https://example.com/image.png | 0:00:10 | 1:00:00 | 1/1/18 0:00 | 12/31/99 23:59 | yes |

**Name** - Name of the image. This value is strictly for reference. <br />
**URL** - Web location of the image file. <br />
**Duration** - Length of time to display the particular image in **H:MM:SS** format. <br />
**Image Description** - Short description of the media, could be displayed in the App <br />
**StartOn** - Date and time to begin showing the specified image in the playlist in **M/d/yy H:mm** format. <br />
**EndBy** - Date and time to stop showing the specified image in the playlist in **M/d/yy H:mm** format. <br />
**Cache** - *Currently Unused* <br />

## Suggested Configuration
It is reccomended that you lock your Apple TV into single app mode with AirPlay Buddy. Images should not be larger than 3840px × 2160px. Loading unessisarily large images may cause the App to crash.

## Contact

Feel free to reach out with any questions, bugs or issues.

## Errors and bugs

If something is not behaving intuitively, it is a bug and should be reported.
Report it here by creating an issue: ./mdm-image-viewer/issues

Help us fix the problem as quickly as possible by following [Mozilla's guidelines for reporting bugs.](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Bug_writing_guidelines#General_Outline_of_a_Bug_Report)

## Patches and pull requests

Your patches are welcome. Here's our suggested workflow:

* Fork the project.
* Make your feature addition or bug fix.
* Send us a pull request with a description of your work. Bonus points for topic branches!

## Special Thanks
AirPlay Buddy is a complete SwiftUI rewrite of the original app written by Quentin Harouff and the University of Nebraska. A special **thank you** for the idea and the initial source code!

## Copyright and attribution

Copyright (c) 2022 RRZE / Friedrich-Alexander-Universität Erlangen_Nürnberg. Released under the [MIT License](LICENSE).
