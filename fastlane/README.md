fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

### getlatestag

```sh
[bundle exec] fastlane getlatestag
```



### sentryUpload

```sh
[bundle exec] fastlane sentryUpload
```



### message_testflightbuild

```sh
[bundle exec] fastlane message_testflightbuild
```



### message_appstoretbuild

```sh
[bundle exec] fastlane message_appstoretbuild
```



----


## tvos

### tvos test

```sh
[bundle exec] fastlane tvos test
```

Build SeminarRoom Buddy for testing

### tvos beta

```sh
[bundle exec] fastlane tvos beta
```

Push a new beta build to TestFlight

### tvos release_not_done_yet

```sh
[bundle exec] fastlane tvos release_not_done_yet
```

Push release build to AppStore

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
