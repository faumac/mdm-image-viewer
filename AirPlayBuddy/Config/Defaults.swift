//
//  Defaults.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 21.11.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import UIKit
import SwiftUI

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Start: Initialize and set defaults
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

var dataURL = "https://faumac.rrze.fau.de/static/mdm-image-viewer/fau-example.json"
var imageTimer: Double = 60
var disableAirplayBoxMovement: Bool = false
var shuffleImages: Bool = true
var airplayViewTimer: Double = 25
var defaultDescription: String = "Wirelessly send what's on your iOS device or computer to this display using AirPlay. Learn more at help.apple.com/appletv."
var defaultSubtitle: String = "CHOOSE THIS APPLE TV"
var deviceName = UIDevice.current.name
var defaultAirPlayText = "AirPlay"
var showClock: Bool = true
let airPlayBoxHeight = 361
let airPlayBoxWidth = 486
var defaultBackground = "Startscreen"
let fauBackground: String = "fau"

let defaultMediaName = "FAU Schloss"
let defaultMediaURL = "https://faumac.rrze.uni-erlangen.de/static/mdm-image-viewer/Schloss.jpg"
let defaultMediaDescription = "Das Schloss der Friedrich-Alexander-Universität Erlangen-Nürnberg"

var backgroundPicture = defaultBackground
var airPlayCornerRadius = 40

var pexelsQuery = "Nature, Forest, beautiful nature, nature wallpaper, universe"
var pexelsMediaCount = 50
var pexelsVideoCount = 30
var useExternalMedia: String = "yes"

var muteVideo: Bool = true
var showVideo: Bool = true

var avatarShowPresence: Bool = true
var avatarSizeX = 90.0
var avatarSizeY = 90.0
var avatarFrameSizeX = 100.0
var avatarFrameSizeY = 100.0
var avatarLiquidSizeX = 170.0
var avatarLiquidSizeY = 170.0
var avatarBackgroundOpacity = 0.5
let avatarStati = [Color.absent, Color.free, Color.call, Color.donotdisturb]
let avatarPresences = [Color.absent, Color.present, Color.homeOffice]
var avatarUserStatusURL = "https://faumac.rrze.uni-erlangen.de/AirPlayBuddy/demoData.json"
var avatarPresenceFetchTimer: Double = 5

extension Color {
    static let absent = Color("Color.absent")
    static let free = Color("Color.free")
    static let call = Color("Color.call")
    static let donotdisturb = Color("Color.donotdisturb")
    static let present = Color("Color.present")
    static let homeOffice = Color("Color.homeOffice")
}
