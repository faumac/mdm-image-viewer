//
//  AppDelegate.swift
//  AirPlayBuddy
//
//  Created by Quentin Harouff on 10/6/18.
//  Copyright © 2018 Quentin Harouff. All rights reserved.
//

import SwiftUI
import GameController
import OSLog
import Sentry
import OSLog

@main
struct MDMAirPlayBuddy: App {
    init() {
#if DEBUG
        Logger.sentry.debug("🐛 Debugging app, not reporting anything to sentry server ...")
#else
        SentrySDK.start { options in
            options.dsn = "https://640356678ed3056f2282f372cdc2acb4@apps.faumac.de:8443/3"
            options.debug = true // Enabling debug when first installing is always helpful

            // Set tracesSampleRate to 1.0 to capture 100% of transactions for tracing.
            // We recommend adjusting this value in production.
            options.tracesSampleRate = 1.0
        }
#endif
    }
    var body: some Scene {
        WindowGroup {
            StartupView()
        }
    }
}

