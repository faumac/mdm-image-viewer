//
//  MediaView.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 22.11.22.
//  Copyright © 2022/2024 RRZE / FAUmac. All rights reserved.
//

import SwiftUI
import GameController
import OSLog
import AVKit
import SentrySwiftUI

struct MediaView: View {
    @ObservedObject var boxAnimation: BoxAnimation
    @StateObject var currentMedia: MediaModel
    @ObservedObject var mediaList: MediaList
    @ObservedObject var presenceModel = PresenceModel()
    
    @State private var counter: Double = 0
    @State private var timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    @State private var videoCounter: Double = 0
    @State private var videoTimer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    @State private var airplayBoxTimer = Timer.publish(every: airplayViewTimer, on: .main, in: .common).autoconnect()
    
    @State private var videoPlayer: AVPlayer?
    
    var body: some View {
        SentryTracedView("MediaView"){
            ZStack {
                Group {
                    if currentMedia.mediaType == .video {
                        VideoPlayerView(currentMedia: currentMedia, videoPlayer: videoPlayer, playVideo: playVideo)
                    } else {
                        AsyncImage(url: URL(string: currentMedia.url)) { phase in
                            switch phase {
                            case .success(let image):
                                image.resizable()
                                    .ignoresSafeArea()
                                    .frame(width: boxAnimation.screenWidth, height: boxAnimation.screenHeight)
                                    .onAppear {
                                        Logger.mediaview.error("Image loaded successfully: \(currentMedia.url, privacy: .public)")
                                    }
                            case .failure:
                                Image(backgroundPicture)
                                    .resizable()
                                    .aspectRatio(contentMode: .fill)
                                    .ignoresSafeArea()
                                    .frame(width: boxAnimation.screenWidth, height: boxAnimation.screenHeight)
                                    .onAppear {
                                        Logger.mediaview.error("Failed to load image: \(currentMedia.url, privacy: .public)")
                                        counter = 99999
                                    }
                            default:
                                ZStack {
                                    Image(backgroundPicture)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .ignoresSafeArea()
                                        .frame(width: boxAnimation.screenWidth, height: boxAnimation.screenHeight)
                                    ProgressView()
                                }
                            }
                        }
                        .onAppear {
                            Logger.mediaview.info("Attempting to load image: \(currentMedia.url, privacy: .public)")
                        }
                        .onChange(of: currentMedia.url) { newValue in
                            Logger.mediaview.debug("URL changed, new value: \(newValue, privacy: .public)")
                        }
                    }
                }
                
                if !currentMedia.description.isEmpty {
                    MediaDescriptionView(currentMedia: currentMedia)
                        .frame(maxWidth: CGFloat.infinity,
                               maxHeight: CGFloat.infinity,
                               alignment: Alignment.bottomTrailing)
                }
                
                if avatarShowPresence {
                    AvatarBoxView()
                        .frame(maxWidth: .infinity,
                               maxHeight: .infinity,
                               alignment: .topLeading)
                        .padding()
                }
                
                AirPlayView()
                    .position(x: CGFloat(boxAnimation.xPosition), y: CGFloat(boxAnimation.yPosition))
                    .onReceive(airplayBoxTimer) { _ in
                        withAnimation(.easeInOut(duration: 5.0)) {
                            boxAnimation.moveBox()
                        }
                    }
            }
        }
        .onReceive(timer) { _ in
            if currentMedia.mediaType == .image {
                let mediaDuration = currentMedia.duration ?? imageTimer
                
                if counter >= mediaDuration {
                    loadNextMedia()
                } else {
                    counter += 1
                }
            }
        }
        .onReceive(videoTimer) { _ in
            if currentMedia.mediaType == .video {
                if let duration = currentMedia.duration {
                    if videoCounter >= duration {
                        loadNextMedia()
                    } else {
                        videoCounter += 1
                    }
                }
            }
        }
        .onDisappear {
            timer.upstream.connect().cancel()
            videoTimer.upstream.connect().cancel()
            videoPlayer?.pause()
        }
        .swipeGestures(onRight: {
            Logger.mediaview.debug("Got a swipe gesture to the right")
            loadNextMedia()
        }, onLeft: {
            Logger.mediaview.debug("Got a swipe gesture to the left")
            loadPreviousMedia()
        })
        .onAppear { UIApplication.shared.isIdleTimerDisabled = true }
        .onDisappear { UIApplication.shared.isIdleTimerDisabled = false }
    }
    
    private func playVideo(url: String) {
        guard let videoURL = URL(string: url) else { return }
        videoPlayer = AVPlayer(url: videoURL)
        videoPlayer?.play()
        videoPlayer?.isMuted = muteVideo
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: videoPlayer?.currentItem, queue: .main) { _ in
            if currentMedia.duration == nil {
                loadNextMedia()
            }
        }
    }
    
    private func loadNextMedia() {
        Task {
            let newMedia = await mediaList.nextMedia(mediaList: mediaList)
            await updateCurrentMedia(with: newMedia)
        }
    }
    
    private func loadPreviousMedia() {
        Task {
            let newMedia = await mediaList.previousMedia(mediaList: mediaList)
            await updateCurrentMedia(with: newMedia)
        }
    }
    
    private func updateCurrentMedia(with newMedia: MediaModel) async {
        await MainActor.run {
            currentMedia.description = newMedia.description
            currentMedia.url = newMedia.url
            currentMedia.name = newMedia.name
            currentMedia.mediaType = newMedia.mediaType
            currentMedia.duration = newMedia.duration
            Logger.mediaview.debug("New media loaded: \(currentMedia.url)")
            counter = 0
            videoCounter = 0
        }
    }
}

struct MediaView_Previews: PreviewProvider {
    static var previews: some View {
        let boxAnimation = BoxAnimation()
        let currentMedia = MediaModel(
            name: "Preview Media",
            url: defaultMediaURL,
            description: defaultMediaDescription,
            mediaType: .image,
            duration: imageTimer
        )
        let mediaList = MediaList(previewData: [
            MediaModel(name: "Preview Media 1", url: "defaultMediaURL", description: "Description 1", mediaType: .image, duration: imageTimer),
            MediaModel(name: "Preview Media 2", url: "defaultMediaURL", description: "Description 2", mediaType: .image, duration: imageTimer)
        ])
        MediaView(boxAnimation: boxAnimation, currentMedia: currentMedia, mediaList: mediaList)
    }
}

extension AVPlayerViewController {
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.showsPlaybackControls = false
    }
}

struct VideoPlayerView: View {
    @ObservedObject var currentMedia: MediaModel
    var videoPlayer: AVPlayer?
    var playVideo: (String) -> Void
    
    var body: some View {
        VideoPlayer(player: videoPlayer)
            .ignoresSafeArea()
            .frame(width: UIScreen.screenWidth, height: UIScreen.screenHeight)
            .onAppear {
                Logger.mediaview.info("Attempting to load video: \(currentMedia.url, privacy: .public)")
                playVideo(currentMedia.url)
            }
            .onChange(of: currentMedia.url) { newValue in
                Logger.mediaview.debug("Video URL changed, new value: \(newValue, privacy: .public)")
                playVideo(newValue)
            }
    }
}
