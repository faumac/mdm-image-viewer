//
//  AvatarView.swift
//  SeminarRoom Buddy
//
//  Created by Gregor Longariva on 16.04.23.
//  Copyright © 2023 RRZE / FAUmac. All rights reserved.
//

import SwiftUI
import Liquid
import SentrySwiftUI

struct AvatarView: View {
    @Binding var presence: Presence 
    
    var body: some View {
        SentryTracedView("AvatarView"){
            HStack {
                ZStack {
                    Liquid()
                        .frame(width: avatarLiquidSizeX, height: avatarLiquidSizeX)
                        .foregroundColor(avatarPresences[presence.presence])
                        .opacity(0.3)
                    Liquid(samples: 99)
                        .frame(width: avatarLiquidSizeX-20, height: avatarLiquidSizeX-20)
                        .foregroundColor(avatarPresences[presence.presence])
                        .opacity(0.5)
                    Liquid(samples: 4)
                        .frame(width: avatarLiquidSizeX-50, height: avatarLiquidSizeX-50)
                        .foregroundColor(avatarStati[presence.status])
                        .opacity(0.8)
                    HStack {
                        AsyncImage(
                            url: URL(string: presence.image),
                            content: { image in
                                image.resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(maxWidth: avatarSizeX, maxHeight: avatarSizeY)
                                //                                 .clipShape(Circle())
                            },
                            placeholder: {
                                ProgressView()
                            }
                        )
                    }.frame(maxWidth: avatarFrameSizeX, maxHeight: avatarFrameSizeY)
                }
            }
        }
    }
}

struct AvatarView_Previews: PreviewProvider {
    static var previews: some View {
        let presence = Presence(id: "12345", name: "Friedrich", image: "memoji", presence: 0, status: 0)
        AvatarView(presence: .constant(presence))
    }
}

