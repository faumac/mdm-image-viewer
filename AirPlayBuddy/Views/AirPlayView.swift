//
//  AirPlayView.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 22.11.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import SwiftUI
import SentrySwiftUI

struct AirPlayView: View {
    @State var airplaySubtitle = defaultSubtitle
    @State var airplayDescription = defaultDescription
    @State var deviceNameLabel = deviceName
    @State var AirPlayText = defaultAirPlayText
    @State var displayClock = showClock

    var body: some View {
        SentryTracedView("AirPlayView"){
            ZStack {
                // First draw the background shape
                RoundedRectangle(cornerRadius: CGFloat(airPlayCornerRadius))
                    .fill(.thinMaterial) // Properly use `.fill` with material
                    .frame(width: CGFloat(airPlayBoxWidth), height: CGFloat(airPlayBoxHeight))
                
                // Then draw contents over it
                VStack(alignment: .leading) {
                    HStack {
                        Image(systemName: "airplayvideo")
                            .font(.system(size: 55))
                            .foregroundColor(Color.white)
                        Text(AirPlayText)
                            .font(.system(size: 55))
                            .foregroundColor(Color.white)
                    }
                    .padding(.bottom)
                    
                    Text(airplayDescription)
                        .font(.system(size: 19))
                        .foregroundColor(Color.white)
                        .padding(.bottom)
                    
                    VStack(alignment: .leading) {
                        Text(airplaySubtitle)
                            .font(.system(size: 19, weight: .bold))
                            .padding(.bottom)
                            .foregroundColor(Color.white)
                        Text(deviceNameLabel)
                            .font(.system(size: 32, weight: .heavy))
                            .foregroundColor(Color.white)
                    }
                    
                    if displayClock {
                        ClockView()
                            .frame(maxWidth: .infinity, alignment: .trailing)
                    }
                }
                .padding(.top, 20)
                .padding(.bottom, 20)
                .padding(.horizontal, 30)
                .frame(width: CGFloat(airPlayBoxWidth), height: CGFloat(airPlayBoxHeight))
            }
        }
    }
}

struct AirPlayView_Previews: PreviewProvider {
    static var previews: some View {
        AirPlayView()
    }
}
