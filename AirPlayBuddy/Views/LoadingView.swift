//
//  LoadingView.swift
//  SeminarRoom Buddy
//
//  Created by Longariva, Gregor (RRZE) on 12.09.24.
//  Copyright © 2024 RRZE / FAUmac. All rights reserved.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        VStack {
            Text("Loading...")
                .font(.largeTitle)
                .padding()
            ProgressView()
        }
    }
}

#Preview {
    LoadingView()
}
