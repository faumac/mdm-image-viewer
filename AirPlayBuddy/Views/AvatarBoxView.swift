//
//  AvatarBoxView.swift
//  SeminarRoom Buddy
//
//  Created by Gregor Longariva on 21.04.23.
//  Copyright © 2023 RRZE / FAUmac. All rights reserved.
//

import SwiftUI
import OSLog
import SentrySwiftUI

struct AvatarBoxView: View {
    @StateObject var presenceModel = PresenceModel()

    // Timer, der alle 5 Sekunden feuert
    @State private var avatarPresenceTimer = Timer.publish(every: 10, on: .main, in: .common).autoconnect()
    
    var body: some View {
        SentryTracedView("AvatarBoxView"){
            HStack {
                ForEach($presenceModel.presence, id: \.self) { $avatarPresence in
                    AvatarView(presence: $avatarPresence)
                }
            }
        }
        .onReceive(avatarPresenceTimer) { _ in
            Task {
                do {
                    try await presenceModel.fetch(withURL: avatarUserStatusURL)
                } catch {
                    Logger.avatarBoxView.error("Fehler beim Abrufen der Daten von \(avatarUserStatusURL, privacy: .public): \(error.localizedDescription, privacy: .public)")
                }
            }
        }
        .onDisappear {
            avatarPresenceTimer.upstream.connect().cancel() // Timer stoppen, wenn die View verschwindet
        }
        .onAppear() {
            Task {
                do {
                    try await presenceModel.fetch(withURL: avatarUserStatusURL)
                } catch {
                    Logger.avatarBoxView.error("Fehler beim Abrufen der Daten von \(avatarUserStatusURL, privacy: .public): \(error.localizedDescription, privacy: .public)")
                }
            }
        }
    }
}

struct AvatarBoxView_Previews: PreviewProvider {
    static var previews: some View {
        AvatarBoxView()
    }
}
