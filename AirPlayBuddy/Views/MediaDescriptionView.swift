//
//  MediaDescriptionView.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 23.11.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import SwiftUI

struct MediaDescriptionView: View {
    @ObservedObject var currentMedia: MediaModel
    
    var body: some View {
        HStack {
            VStack {
                Text(currentMedia.description)
                    .italic()
                    .font(.system(size: 19))
            }
            .padding()
            .background(.regularMaterial)
            .cornerRadius(10)
        }
    }
}

struct MediaDescriptionView_Previews: PreviewProvider {
    static var previews: some View {
        let currentMedia = MediaModel(
            name: "Preview Media",
            url: defaultMediaURL,
            description: defaultMediaDescription,
            mediaType: .image,
            duration: nil
        )
        MediaDescriptionView(currentMedia: currentMedia)
    }
}
