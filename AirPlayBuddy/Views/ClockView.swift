//
//  ClockView.swift
//  AirPlay Buddy
//
//  Created by Gregor Longariva on 06.12.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import SwiftUI

struct ClockView: View {
    @State var currentDate = Date.now
    let clockTimer = Timer.publish(every: 10, on: .main, in: .common).autoconnect()

    var body: some View {
        Text(currentDate.formatted(.dateTime.hour().minute()))
            .font(.system(size: 19 ))
            .foregroundColor(Color.white)
            .onReceive(clockTimer) { input in
                    currentDate = input
        }
    }
}

struct ClockView_Previews: PreviewProvider {
    static var previews: some View {
        ClockView()
    }
}
