//
//  StartupView.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 29.11.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import SwiftUI
import GameController
import OSLog
import SentrySwiftUI

struct StartupView: View {
    let miniSleepNanoSeconds: UInt64 = 5_000_000_000
    @StateObject  var boxAnimation = BoxAnimation()
    @StateObject  var mediaList = MediaList()
    @State private var counter: Double = 0
    @State private var isLoading = true
    @State private var loadingError: String?
    @State private var refreshView: Bool = false
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        Group {
            if isLoading {
                LoadingView()
            } else if let error = loadingError {
                Text("Error: \(error)")
                    .foregroundColor(.red)
            } else if mediaList.mediaSource.isEmpty {
                Text("No media available")
                    .font(.largeTitle)
            } else if counter < 1 {
                Image(backgroundPicture)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .ignoresSafeArea()
                    .onReceive(timer) { _ in
                        counter += 1
                        Logger.startupview.info("Startup media list timer fired. Counter: \(counter)")
                    }
            } else {
                MediaView(boxAnimation: boxAnimation, currentMedia: mediaList.mediaSource[0], mediaList: mediaList)
                    .id(refreshView)
            }
        }
        .sentryTrace("Startup View")
        .onAppear {
//            Logger.startupview.debug("✨ StartupView appeared the first time. Media source count: \(mediaList.mediaSource.count)")
//            isLoading = true
//            reloadData()
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.didBecomeActiveNotification)) { _ in
            Logger.startupview.debug("✨ App became active. (Re)loading data.")
            isLoading = true
            reloadData()
            refreshView.toggle()
        }
    }
    
    private func reloadData() {
        Task { @MainActor in
            do {
                try await Task.sleep(nanoseconds: miniSleepNanoSeconds)
                await loadInitialData()
                try await Task.sleep(nanoseconds: miniSleepNanoSeconds)
            } catch {
                Logger.startupview.error("Error during data reload: \(error.localizedDescription)")
            }
        }
    }
    
    private func loadInitialData() async {
        Logger.startupview.info("Loading initial data")
        getManagedAppConfiguration()
        await mediaList.loadMediaList()
        Logger.startupview.info("Media list loaded successfully. Count: \(mediaList.mediaSource.count)")
        isLoading = false
    }
}

#Preview {
    StartupView()
}

