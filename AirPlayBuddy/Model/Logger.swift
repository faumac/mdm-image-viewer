//
//  Logger.swift
//  SeminarRoom Buddy
//
//  Created by Longariva, Gregor (RRZE) on 08.08.24.
//  Copyright © 2024 RRZE / FAUmac. All rights reserved.
//

import OSLog

/// extension to Unified logging
extension Logger {
    /// Using bundle identifier
    private static var subsystem = Bundle.main.bundleIdentifier!
    /// Define different states and tasks of the app
    static let presence = Logger(subsystem: subsystem, category: "presence")
    static let mediaview = Logger(subsystem: subsystem, category: "mediaview")
    static let medialist = Logger(subsystem: subsystem, category: "medialist")
    static let appstate = Logger(subsystem: subsystem, category: "appstate")
    static let startupview = Logger(subsystem: subsystem, category: "startupview")
    static let avatarBoxView = Logger(subsystem: subsystem, category: "avatarBoxView")
    static let boxAnimation = Logger(subsystem: subsystem, category: "boxAnimation")
    static let sentry = Logger(subsystem: subsystem, category: "sentry")
}
