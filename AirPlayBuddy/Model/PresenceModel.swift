//
//  PresenceModel.swift
//  SeminarRoom Buddy
//
//  Created by Gregor Longariva on 18.04.23.
//  Copyright © 2023 RRZE / FAUmac. All rights reserved.
//

import Foundation
import OSLog

struct Presence: Codable, Identifiable, Hashable {
    var id: String
    var name: String
    var image: String
    var presence: Int
    var status: Int
}

final class PresenceModel: ObservableObject {
    @Published var presence = [Presence]()
    private var lastETag: String?
    private let cache = NSCache<NSString, NSArray>()
    private let cacheKey = "presenceCache"
    
    init() {
        loadFromCache()
    }

    func fetch(withURL url: String) async throws {
        do {
//            Logger.presence.debug("Avatar fetch was called.")
            guard let url = URL(string: url) else {
                Logger.presence.error("Decoding of url \(url, privacy: .public) failed.")
                throw HTTPError.invalidURL
            }
            var request = URLRequest(url: url)
            // avoid cached data
            request.cachePolicy = .reloadIgnoringLocalCacheData
            request.setValue("no-cache", forHTTPHeaderField: "Cache-Control")
            
            if let etag = lastETag {
                request.addValue(etag, forHTTPHeaderField: "If-None-Match")
            }
            let (data, response) = try await URLSession.shared.data(for: request)
            
            guard let httpResponse = response as? HTTPURLResponse else {
                throw HTTPError.invalidResponse
            }
            
            if httpResponse.statusCode == 304 {
                Logger.avatarBoxView.debug("  content hasn't changed.")
                // Content hasn't changed
                return
            }
            
            switch httpResponse.statusCode {
            case 200:
//                Logger.presence.debug("   got 200.")
                if let newETag = httpResponse.allHeaderFields["ETag"] as? String {
                    Logger.presence.debug("   New etag:\(newETag).")
                    lastETag = newETag
                }
                let presences = try JSONDecoder().decode([Presence].self, from: data)
                await MainActor.run {
                    for newPresence in presences {
                        if let index = presence.firstIndex(where: { $0.id == newPresence.id }) {
                            if presence[index] != newPresence {
                                Logger.avatarBoxView.debug("🏢 Presence changed for user: \(newPresence.name)")
                                presence[index] = newPresence
                            }
                        } else {
                            Logger.avatarBoxView.debug("🏢 New presence added for user: \(newPresence.name)")
                            presence.append(newPresence)
                        }
                    }
                    
                    // Entferne Presence-Objekte, die nicht mehr in den empfangenen Daten vorhanden sind
                    presence.removeAll(where: { oldPresence in
                        !presences.contains(where: { $0.id == oldPresence.id })
                    })
                }
            case 304:
                Logger.presence.info("Content not modified")
                throw HTTPError.notModified
            case 300...399:
                Logger.presence.warning("Redirect status: \(httpResponse.statusCode)")
                throw HTTPError.redirectError(httpResponse.statusCode)
            case 400...499:
                Logger.presence.error("Client error: \(httpResponse.statusCode)")
                throw HTTPError.clientError(httpResponse.statusCode)
            case 500...599:
                Logger.presence.error("Server error: \(httpResponse.statusCode)")
                throw HTTPError.serverError(httpResponse.statusCode)
            default:
                Logger.presence.error("Unexpected status code: \(httpResponse.statusCode)")
                throw HTTPError.invalidResponse
            }
        } catch let decodingError as DecodingError {
            Logger.presence.error("Decoding error: \(decodingError)")
            throw HTTPError.invalidData
        } catch let urlError as URLError {
            Logger.presence.error("URL error: \(urlError)")
            throw HTTPError.networkError(urlError)
        } catch {
            Logger.presence.error("Unexpected error: \(error)")
            throw error
        }
    }
    
    private func loadFromCache() {
        if let cachedPresences = cache.object(forKey: cacheKey as NSString) as? [Presence] {
            presence = cachedPresences
        }
    }
    
    private func saveToCache(_ presences: [Presence]) {
        cache.setObject(presences as NSArray, forKey: cacheKey as NSString)
    }
}
