//
//  BoxAnimation.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 22.11.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import Foundation
import OSLog
import UIKit

extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}

class BoxAnimation: ObservableObject {
    let screenWidth = UIScreen.screenWidth
    let screenHeight = UIScreen.screenHeight
    @Published var xPosition = (Int(UIScreen.screenWidth) - airPlayBoxWidth / 2 - 20)
    @Published var yPosition = (Int(UIScreen.screenHeight) - airPlayBoxHeight / 2 - 60)
    
    func moveBox() {
        if !disableAirplayBoxMovement {
            Logger.boxAnimation.debug("Moving Airplay Box.")
            xPosition = Int.random(in: airPlayBoxWidth / 2 + 20 ..< (Int(screenWidth) - airPlayBoxWidth / 2 - 20))
            if avatarShowPresence {
                yPosition = Int.random(in: airPlayBoxHeight / 2 + 60 + (Int(avatarLiquidSizeY)) ..< (Int(screenHeight) - airPlayBoxHeight / 2 - 60))
            } else {
                yPosition = Int.random(in: airPlayBoxHeight / 2 + 60 ..< (Int(screenHeight) - airPlayBoxHeight / 2 - 60))
            }
        }
    }
}
