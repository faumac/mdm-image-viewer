//
//  HTTPError.swift
//  SeminarRoom Buddy
//
//  Created by Longariva, Gregor (RRZE) on 05.09.24.
//  Copyright © 2024 RRZE / FAUmac. All rights reserved.
//

import Foundation

enum HTTPError: LocalizedError {
    case invalidURL
    case networkError(Error)
    case invalidResponse
    case invalidData
    case serverError(Int)
    case clientError(Int)
    case redirectError(Int)
    case notModified
    
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return "Invalid URL"
        case .networkError(let error):
            return "Network error: \(error.localizedDescription)"
        case .invalidResponse:
            return "Invalid response from server"
        case .invalidData:
            return "Invalid data received from server"
        case .serverError(let code):
            return "Server error with status code: \(code)"
        case .clientError(let code):
            return "Client error with status code: \(code)"
        case .redirectError(let code):
            return "Redirect error with status code: \(code)"
        case .notModified:
            return "Content not modified (304)"
        }
    }
}
