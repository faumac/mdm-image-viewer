//
//  ManageApp.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 21.11.22.
//  Copyright © 2022 RRZE / FAUmac. All rights reserved.
//

import UIKit
import Foundation

// ----------------------------------------
// function: getManagedAppConfiguration:
//
// This function looks for managed app configuration values
// loaded to the Apple TV by an MDM. If no values exist, the
// defaults will be used.
//
// ----------------------------------------

func getManagedAppConfiguration() {
    
#if DEBUG
    deviceName = "10 Vorne"
#endif
    
    // Get Managed App Configuration passsed by MDM
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.dataURL") {
        dataURL = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.dataURL") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.imageTimer") {
        imageTimer = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.imageTimer") as! Double
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.disableAirplayBoxMovement") {
        disableAirplayBoxMovement = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.disableAirplayBoxMovement") as! Bool
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.shuffleImages") {
        shuffleImages = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.shuffleImages") as! Bool
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.airplayViewTimer") {
        airplayViewTimer = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.airplayViewTimer") as! Double
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.defaultBackground") {
        defaultBackground = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.defaultBackground") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.defaultSubtitle") {
        defaultSubtitle = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.defaultSubtitle") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.defaultDescription") {
        defaultDescription = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.defaultDescription") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.deviceName") {
        deviceName = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.deviceName") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.deviceName") {
        deviceName = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.deviceName") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.showClock") {
        showClock = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.showClock") as! Bool
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.defaultAirPlayText") {
        defaultAirPlayText = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.defaultAirPlayText") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.useExternalMedia") {
        useExternalMedia = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.useExternalMedia") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.defaultAirPlayText") {
        defaultAirPlayText = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.defaultAirPlayText") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.pexelsAPIKey") {
        pexelsAPIKey = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.pexelsAPIKey") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.pexelsQuery") {
        pexelsQuery = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.pexelsQuery") as! String
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.pexelsMediaCount") {
        pexelsMediaCount = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.pexelsMediaCount") as! Int
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.pexelsVideoCount") {
        pexelsVideoCount = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.pexelsVideoCount") as! Int
    }
    
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarShowPresence") {
        avatarShowPresence = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarShowPresence") as! Bool
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarSizeX") {
        avatarSizeX = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarSizeX") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarSizeY") {
        avatarSizeY = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarSizeY") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarFrameSizeX") {
        avatarFrameSizeX = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarFrameSizeX") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarFrameSizeY") {
        avatarFrameSizeY = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarFrameSizeY") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarLiquidSizeX") {
        avatarLiquidSizeX = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarLiquidSizeX") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarLiquidSizeY") {
        avatarLiquidSizeY = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarLiquidSizeY") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarBackgroundOpacity") {
        avatarBackgroundOpacity = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarBackgroundOpacity") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarUserStatusURL") {
        avatarUserStatusURL = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarUserStatusURL") as! String
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.avatarPresenceFetchTimer") {
        avatarPresenceFetchTimer = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.avatarPresenceFetchTimer") as! Double
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.muteVideo") {
        muteVideo = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.muteVideo") as! Bool
    }
    if keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.showVideo") {
        showVideo = ManagedAppConfig.shared.getConfigValue(forKey: "de.fau.rrze.AirPlayBuddy.showVideo") as! Bool
    }
    
// Color defiition ist done by xcode assets, could also be configured by MDM profile one day
//    let avatarStati = [Color.absent, Color.free, Color.call, Color.donotdisturb]
//    let avatarPresences = [Color.absent, Color.present, Color.homeOffice]
    
    
    
    
    
}

// ----------------------------------------
// function: keyPresentInUserDefaults:
//
// Uses the provided string key value to check if a
// configuration value was provided by MDM and returns
// true or false.
//
// useage example: keyPresentInUserDefaults(key: "de.fau.rrze.AirPlayBuddy.dataURL")
//
// ----------------------------------------

func keyPresentInUserDefaults(key: String) -> Bool {
    return ManagedAppConfig.shared.getConfigValue(forKey: key) != nil
}
