//
//  ImageModel.swift
//  MDM image Viewer
//
//  Created by Gregor Longariva on 21.11.22.
//  Copyright © 2022/2024 RRZE / FAUmac. All rights reserved.
//

import Foundation

enum MediaType: String, Codable {
    case image
    case video
}

class MediaModel: ObservableObject, Codable {
    @Published var name: String
    @Published var url: String
    @Published var description: String
    @Published var mediaType: MediaType
    @Published var duration: TimeInterval?
    
    enum CodingKeys: String, CodingKey {
        case name
        case url
        case description
        case mediaType
        case duration
    }
    
    init(name: String, url: String, description: String, mediaType: MediaType, duration: TimeInterval?) {
        self.name = name
        self.url = url
        self.description = description
        self.mediaType = mediaType
        self.duration = duration
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        url = try container.decode(String.self, forKey: .url)
        description = try container.decode(String.self, forKey: .description)
        mediaType = try container.decode(MediaType.self, forKey: .mediaType)
        duration = try container.decodeIfPresent(TimeInterval.self, forKey: .duration)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(url, forKey: .url)
        try container.encode(description, forKey: .description)
        try container.encode(mediaType, forKey: .mediaType)
        try container.encodeIfPresent(duration, forKey: .duration)
    }
}
