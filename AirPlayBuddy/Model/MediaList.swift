//
//  MediaList.swift
//  SeminarRoom Buddy
//
//  Created by Longariva, Gregor (RRZE) on 10.07.24.
//  Copyright © 2024 RRZE / FAUmac. All rights reserved.
//

import Foundation
import PexelsSwift
import OSLog

class MediaList: ObservableObject {
    @Published var mediaSource: [MediaModel] = []
    @Published var mediaCounter = 0
    private var privateMediaSource: [MediaModel] = []
    var fetchPage = 0
    let pexels = PexelsSwift.shared
    
    init() {
        pexels.setup(apiKey: pexelsAPIKey)
    }
    
    init(withUrl mediaURL: String) {
        pexels.setup(apiKey: pexelsAPIKey)
        Task { await loadMediaList() }
    }
    
    init(previewData: [MediaModel]) {
        self.mediaSource = previewData
        pexels.setup(apiKey: pexelsAPIKey)
    }
    
    func loadMediaList() async {
        do {
            let newMediaSource: [MediaModel] = try await fetchMediaSource()
            
            await MainActor.run {
                self.mediaSource = newMediaSource.shuffled()
                self.privateMediaSource = self.mediaSource
            }
        } catch {
            Logger.medialist.error("Error loading media list: \(error.localizedDescription)")
            await MainActor.run {
                let errorMedia = createErrorMedia(description: "Error: Failed to load media list")
                self.mediaSource = [errorMedia]
                self.privateMediaSource = self.mediaSource
            }
        }
    }
    
    private func fetchMediaSource() async throws -> [MediaModel] {
        switch useExternalMedia.lowercased() {
        case "no":
            return try await loadJSONList(from: dataURL)
        case "yes":
            self.fetchPage += 1
            let photos = await getPexelsList()
            try? await Task.sleep(for: .seconds(2))
            let videos = await getPexelsVideoList()
            return photos + (showVideo ? videos : [])
        default:
            var combinedSource = try await loadJSONList(from: dataURL)
            self.fetchPage += 1
            let photos = await getPexelsList()
            try? await Task.sleep(for: .seconds(2))
            let videos = await getPexelsVideoList()
            combinedSource += photos + (showVideo ? videos : [])
            return combinedSource
        }
    }
    
    private func loadJSONList(from urlString: String) async throws -> [MediaModel] {
        Logger.medialist.info("Attempting to load media list from URL: \(urlString)")
        guard let url = URL(string: urlString) else {
            Logger.medialist.error("Invalid URL: \(urlString)")
            throw URLError(.badURL)
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        Logger.medialist.info("JSON Data fetched successfully. Size: \(data.count) bytes")
        
        let mediaObjects = try parseJSON(data: data)
        Logger.medialist.info("JSON parsed successfully. Media objects count: \(mediaObjects.count)")
        
        return mediaObjects
    }
    
    private func parseJSON(data: Data) throws -> [MediaModel] {
        let decoder = JSONDecoder()
        let mediaObjects = try decoder.decode([MediaModel].self, from: data)
        
        if showVideo {
            return mediaObjects
        } else {
            return mediaObjects.filter { $0.mediaType != .video }
        }
    }
    
    private func getPexelsVideoList() async -> [MediaModel] {
        Logger.medialist.info("Attempting to load Pexels video list")
        let result = await pexels.searchVideos(pexelsQuery,
                                               orientation: PexelsSwift.PSOrientation.landscape,
                                               size: .large,
                                               page: self.fetchPage,
                                               count: pexelsVideoCount)
        switch result {
        case .success(let (videos, _, _)):
            return videos.compactMap { video in
                // Filtere die VideoFiles nach Qualität und Querformat
                if let bestFile = video.videoFiles.first(where: { file in
                    let isQualitySufficient = file.quality == .uhd || file.quality == .hd
                    let isLandscape = (file.width ?? 0) > (file.height ?? 0)
                    return isQualitySufficient && isLandscape
                }) {
                    return MediaModel(
                        name: String(video.id),
                        url: bestFile.link,
                        description: "",
                        mediaType: .video,
                        duration: TimeInterval(video.duration)
                    )
                }
                return nil
            }
        case .failure(let error):
            Logger.medialist.error("Error on getting external video media: \(error.localizedDescription)")
            return [createErrorMedia(description: "Error on getting external video media. Please check your config")]
        }
    }
    
    private func getPexelsList() async -> [MediaModel] {
        Logger.medialist.info("Attempting to load Pexels pictures list")
        let result = await pexels.searchPhotos(pexelsQuery,
                                               orientation: PexelsSwift.PSOrientation.landscape,
                                               size: .large,
                                               page: self.fetchPage,
                                               count: pexelsMediaCount)
        switch result {
        case .success(let (photos, _, _)):
            return photos.map { photo in
                MediaModel(
                    name: String(photo.id),
                    url: photo.source[PSPhoto.Size.large2x.rawValue] ?? "",
                    description: photo.alternateDescription,
                    mediaType: .image,
                    duration: nil
                )
            }
        case .failure(let error):
            Logger.medialist.error("Error on getting external media: \(error.localizedDescription)")
            return [createErrorMedia(description: "Error on getting external media. Please check your config")]
        }
    }
    
    func previousMedia(mediaList: MediaList) async -> MediaModel {
        await MainActor.run {
            self.mediaCounter -= 1
            if self.mediaCounter < 0 {
                self.mediaCounter = mediaList.mediaSource.count - 1
            }
            return mediaList.mediaSource[self.mediaCounter]
        }
    }
    
    func nextMedia(mediaList: MediaList) async -> MediaModel {
        await MainActor.run {
            self.mediaCounter += 1
            
            if self.mediaCounter >= self.privateMediaSource.count {
                Task {
                    await self.loadMediaList()
                }
                self.mediaCounter = 0
            }
            return mediaList.mediaSource[self.mediaCounter]
        }
    }
    
    private func createErrorMedia(description: String) -> MediaModel {
        MediaModel(
            name: "Seminar Room Buddy",
            url: defaultBackground,
            description: description,
            mediaType: .image,
            duration: nil
        )
    }
}
