//
//  PexelsView.swift
//  SeminarRoom Buddy
//
//  Created by Gregor Longariva on 16.01.23.
//  Copyright © 2023 RRZE / FAUmac. All rights reserved.
//

import SwiftUI
import PexelsSwift

struct PexelsView: View {
    @StateObject private var model = PexelsModel.shared
    
    var body: some View {
        ScrollView {
            LazyVStack(spacing: 0) {
                    LazyVStack(spacing: 0) {
                        ForEach(model.curatedImages) { photo in
                            PhotoCard(photo: photo)
                                .onAppear {
                                    if photo == model.curatedImages.last {
                                        model.getCuratedImages(nextPage: true)
                                    }
                                }
                        }
                }
            }
        }
    }
}

struct PexelsView_Previews: PreviewProvider {
    static var previews: some View {
        PexelsView()
    }
}

//
//  PhotoCard.swift
//  PexelsBrowser
//
//  Created by Lukas Pistrol on 15.11.21.
//

import SwiftUI
import PexelsSwift

struct PhotoCard: View {
    @StateObject private var model = PexelsModel.shared
    
    var photo: PSPhoto
    
    @State private var showDetails: Bool = false
    @State private var downloading: Bool = false
    
    var body: some View {
        image
        .ignoresSafeArea()
        .onChange(of: model.showNotification) { newValue in
            if newValue == true {
                downloading = false
            }
        }
    }
    
    var image: some View {
        AsyncImage(url: URL(string: "https://images.pexels.com/photos/1408221/pexels-photo-1408221.jpeg"),
                   transaction: Transaction(animation: .easeInOut)) { phase in
            switch phase {
            case .empty:
                ZStack {
                    Rectangle().foregroundColor(.init(photo.averageColor))
                    ProgressView()
                }.aspectRatio(Double(photo.width)/Double(photo.height), contentMode: .fit)
            case .success(let image):
                image
                    .resizable()
                    .scaledToFit()
                    .ignoresSafeArea()
            case .failure(_):
                ZStack {
                    Rectangle().foregroundColor(.init(photo.averageColor))
                    Image(systemName: "wifi.exclamationmark")
                        .font(.largeTitle)
                }.aspectRatio(Double(photo.width)/Double(photo.height), contentMode: .fit)
            @unknown default: EmptyView()
            }
        }
        .frame(maxWidth: .infinity)
    }
}


